﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceRequest
    {
        [Required]
        [StringLength(500)]
        public string Name { get; set; }
    }
}