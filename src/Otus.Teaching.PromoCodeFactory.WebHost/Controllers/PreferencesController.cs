﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        
        public PreferencesController(IRepository<Preference> preferenseRepository)
        {
            _preferenceRepository = preferenseRepository;
        }

        /// <summary>
        /// Получить данные всех прeдпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetPreferenceAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = preferences.Select(x =>
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            return response;
        }
        /// <summary>
        /// Получить предпочтение по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            return ConvertToPreferenceResponse(preference);
        }

        /// <summary>
        /// Добавить новоe предпочтение
        /// </summary>
        /// <returns></returns>               
        [HttpPost]
        public async Task<ActionResult<PreferenceResponse>> AddPreferenceAsync([FromBody] PreferenceRequest request)
        {
            // проверить что такое уже есть
            var preference = await _preferenceRepository.GetFirstWhere(x => x.Name == request.Name);
            
            if (preference == null) 
            {
                preference = new Preference()
                {
                    Name = request.Name
                };

                preference = await _preferenceRepository.AddAsync(preference);
            }

            return ConvertToPreferenceResponse(preference);
        }

        /// <summary>
        /// Изменить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> UpdatePreferenceAsync(Guid id, PreferenceRequest request)
        {
            Preference preference = await _preferenceRepository.GetByIdAsync(id);
            if(preference == null)
                return NotFound(preference);

            // проверить что такое уже есть
            var preference2 = await _preferenceRepository.GetFirstWhere(x => x.Name.Equals(request.Name));
            if (preference2 != null)
                return BadRequest(preference);

            preference.Name = request.Name;
            preference = await _preferenceRepository.UpdateAsync(preference);
            return ConvertToPreferenceResponse(preference);
        }

        /// <summary>
        /// Удалить предпочтение по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeletePreference(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                return NotFound();

            await _preferenceRepository.DeleteAsync(id);

            return Ok();
        }

        private static PreferenceResponse ConvertToPreferenceResponse(Preference preference)
        {
            var model = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name    
            };

            return model;
        }


    }
}
