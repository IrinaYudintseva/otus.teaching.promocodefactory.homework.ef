﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;


namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить данные роли по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleItemResponse>> GetRoleByIdAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            return CreateRoleItemResponse(role);
        }

        /// <summary>
        /// Добавить новую роль 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<RoleItemResponse>> AddRoleAsync([FromBody] RoleItemRequest roleData)
        {
            var new_role = new Role()
            {
                Name = roleData.Name,
                Description = roleData.Description
            };

            var role = await _rolesRepository.AddAsync(new_role);
            if (role == null)
                return BadRequest();

            return CreateRoleItemResponse(role);
        }

        /// <summary>
        /// Удалить роль по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteRoleAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);
            if (role == null)
                return NotFound();

            await _rolesRepository.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Изменить данные роли 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<RoleItemResponse>> UpdateRoleAsync(Guid id, [FromBody] RoleItemRequest roleData)
        {
            var updRole = await _rolesRepository.GetByIdAsync(id);
            if (updRole == null)
                return NotFound();

            if (updRole.Name.Equals(roleData.Name, StringComparison.Ordinal) &&
                updRole.Description.Equals(roleData.Description, StringComparison.Ordinal))
                return CreateRoleItemResponse(updRole);

            updRole.Name = roleData.Name;
            updRole.Description = roleData.Description;
            updRole = await _rolesRepository.UpdateAsync(updRole);
            if (updRole == null)
                return BadRequest();

            return CreateRoleItemResponse(updRole);
        }

        private static RoleItemResponse CreateRoleItemResponse(Role role)
        {
            var roleModel = new RoleItemResponse()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description
            };

            return roleModel;
        }

    }
}