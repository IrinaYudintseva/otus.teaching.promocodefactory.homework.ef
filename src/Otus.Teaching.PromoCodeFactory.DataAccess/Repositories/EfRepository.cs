﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private readonly DataContext _db;
        public EfRepository(DataContext context)
        {
            _db = context;
        }

        public void Dispose() 
        {
            //TODO
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            //TODO
            var entities = await _db.Set<T>().ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids) 
        {
            var entities = await _db.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }
        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate) 
        {
            return await _db.Set<T>().FirstOrDefaultAsync(predicate);
        }
        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate) 
        {
            return await _db.Set<T>().Where(predicate).ToListAsync();
        }
        public async Task<T> AddAsync(T item)
        {
            await _db.Set<T>().AddAsync(item);
            await _db.SaveChangesAsync();

            return await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == item.Id);
        }

        public async Task<T> UpdateAsync(T item)
        {
            await _db.SaveChangesAsync();
            return await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == item.Id);
        }

        public async Task DeleteAsync(Guid id)
        {
            var item = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (item == null) 
                return;

            _db.Set<T>().Remove(item);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T item)
        {
            _db.Set<T>().Remove(item);
            await _db.SaveChangesAsync();
        }

        public async Task SaveAsync(T item) 
        {
            await _db.SaveChangesAsync(true, default);
        }

    }
}